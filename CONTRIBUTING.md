**How To Cite A Book Right**

Are you struggling to [cite](https://www.pcc.edu/library/research/reasons-for-citing-sources/) a book correctly?
If so, then you’ve come to the right place.
In this article, we’re going to teach you how to do it.
Read on.
As usual, when writing an academic paper, you’re going to use words or ideas from outside sources. This could be from a book, articles, website, etc.
As a rule, you’re required to let your readers know what content you have picked from other sources.
Typically, you’ll add an in-text citation at the end of the sentence in which you have paraphrased or quoted someone else’s work.
You will then have a reference list corresponding to the in-text citations at the end of your paper- because even if you don’t, you’re going to face plagiarism.
But how do you cite a book correctly?
For your information, <a href="https://edubirdie.com/citation/mla/cite-a-book/">citing a book in MLA 8th for free at Edubirdie</a> is now possible.
Let’s see how to do it below:

**Step #1: Identifying the Components of a Book Citation**
First off, when it comes to how to cite a book correctly, you need to identify the pieces of information that MUST be included in your citation. Some content isn’t a must to be cited.
And some pieces of information aren’t part of a book citation.
As stated above, a citation is meant to help the reader find the source easily.
So in your book citation, you need to provide the necessary information and leave out the redundant ones.
The author of the book and its title are important pieces of information to include in your citation.
The year of publication and location should also be included when citing a book.
The academic qualifications or title and other details of the author shouldn’t be included in a book citation.

**Step #2: Choose a Citation Style**
Once you have identified the pieces of information to include in your book citation, the next step is to choose a citation style.
The three major citation styles are APA, MLA, and Chicago style.
The APA style is commonly used in Science, Education, and Psychology.
The MLA style is majorly used in Humanities whereas the Chicago style is used when citing information in History, Business, and Fine Arts.

**How to Cite a Book in APA Style**
Include the author’s last name and date of publication: In your reference list, start with the author’s last name, then a comma and space, and then include the author’s initials.
In the case of multiple authors, separate their last names with comas with an ampersand (&) before the last name of the author. Include the year of publication in parenthesis then a period at the end of the closing parenthesis.
**_Example_:** Jonathan, T. R., & Lauren, R. J. (1901).
Include the title of the book in italics: Provide the title of the book in sentence case in italics capitalizing only the first word and proper nouns in the title.
For a book with a title and subtitle, add a colon to separate the two, and then include a period in the end.
**_Example_**: Jonathan, T. R., & Lauren, R. J. (1901). The definitive guide to planting trees in the desert.
Provide the location and name of the publisher at the end.
Example: Jonathan, T. R., & Lauren, R. J. (1901). The definitive guide to planting trees in the desert. New York: American Environmental Association.
You’ll then use the author’s last name and date of publication in the in-text citation.
**_Example_**: Painting trees in the desert could help to provide food for starving animals (Jonathan & Lauren, (1901).

**How to Cite a Book in MLA**
Start with the author’s name in your Works Cited: Start with the author’s last name, followed by a comma, space, and the author’s first name.
In the case of 2 or 3 authors, use commas to separate the names, with the word “and” before the last author’s name.
In the case of more than 3 authors, include the first author’s name; followed by a comma, and “et. Al.)
Include the book’s title in italics: Provide the title of the book, capitalizing the first word of every letter.
If the book has a title and subtitle, use a colon and a space to separate the two, and then include a period in the end.
Provide the publisher and year of publication: Enter the book publisher, followed by a comma and space then the year the book was published, and a period in the end.
**_Example_**e: Jonathan, Lauren. Defining Nature from a Science Perspective. Penguin, 1901.
Provide the author’s name and page number in your in-text citation
**_Example_**: Sometimes defining nature can sound like shooting in the dark (Jonathan 12).

**How to Cite a Book in Chicago Style**
Use the name of the author or authors: Start your [bibliography](https://guides.lib.uw.edu/c.php?g=344197&p=2318401) with the author’s name, followed by a comma and space, then author’s first name, middle name, or initial, if provided.
In the case of multiple authors, reverse the order of the first author’s name, separate the names with commas, with the word “and” before the last author’s name, and a period in the end.
**_Example_**: Johansen, Mock Y., and Person T. Douglas.
Type the title of the book in italics: Provide the title of the book, capitalizing only the first word, verbs, pronouns, and adverbs.
Use a colon to separate it with the subtitle if the book has both, ensuring the subtitle is in title case, then use a period in the end.
**_Example_**: Johansen, Mock Y., and Person T. Douglas. The Year it Rained Peace in Somalia.
Provide the place of publication followed by a colon and space.
**_Example_**: Johansen, Mock Y., and Person T. Douglas. The Year it Rained Peace in Somalia. Oxford: Oxford University Press, 2001.
